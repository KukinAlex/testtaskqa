﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace TestTaskQA.Pages
{
    class HomePage
    {
        /// <summary>Ссылка на погоду по часам текущего дня</summary>
        [FindsBy(How = How.LinkText, Using = "Почасовой прогноз")]
        public IWebElement LinkToDetailsWeatherToday { get; set; }
        
        /// <summary>Ссылка яндекс директ</summary>
        [FindsBy(How = How.LinkText, Using = "Яндекс.Директ")]
        public IWebElement DirectLink { get; set; }

        /// <summary>Ссылка погода в мире</summary>
        [FindsBy(How = How.LinkText, Using = "Погода в мире")]
        public IWebElement WorldWeatherLink { get; set; }

        /// <summary>Логотип Яндекс с выпадающим меню</summary>
        [FindsBy(How = How.CssSelector, Using = "html body div div div div")]
        public IWebElement LogoMenyElement { get; set; }

        /// <summary>Текстовое поле выбора города</summary>
        [FindsBy(How = How.Id, Using = "header2input")]
        public IWebElement CityTextElement { get; set; }
    }
}
