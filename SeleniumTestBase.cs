﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;

namespace TestTaskQA
{
    [TestClass]
    public abstract class SeleniumTestBase
    {
        protected IWebDriver Driver { get; set; }

        [TestInitialize]
        public abstract void TestInitialize();

        [TestCleanup]
        public virtual void TestCleanup()
        {
            Driver.Quit();
        }
    }
}
