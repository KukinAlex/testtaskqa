﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.PageObjects;
using TestTaskQA.Pages;

namespace TestTaskQA
{
    [TestClass]
    public class HomePageTests : SeleniumTestBase
    {
        FirefoxDriver _driver;
        HomePage _pageHome;

        [TestInitialize]
        public override void TestInitialize()
        {
            _driver = new FirefoxDriver();
            _pageHome = new HomePage();
            PageFactory.InitElements(_driver, _pageHome);
        }

        [TestCleanup]
        public override void TestCleanup()
        {
            _driver.Quit();
        }

        [TestMethod]
        public void TestDetailWeatherTodayLink()
        {
            _driver.Navigate().GoToUrl("https://yandex.ru/pogoda/");
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            _pageHome.LinkToDetailsWeatherToday.Click();
        }

        [TestMethod]
        public void TestDirectLink()
        {
            _driver.Navigate().GoToUrl("https://yandex.ru/pogoda/");
            _pageHome.DirectLink.Click();
        }

        [TestMethod]
        public void TestWorldWeatherLink()
        {
            _driver.Navigate().GoToUrl("https://yandex.ru/pogoda/");
            _pageHome.WorldWeatherLink.Click();
        }

        [TestMethod]
        public void TestLogoMeny()
        {
            _driver.Navigate().GoToUrl("https://yandex.ru/pogoda/");
            _pageHome.LogoMenyElement.Click();
        }

        [TestMethod]
        public void TestCityChange()
        {
            _driver.Navigate().GoToUrl("https://yandex.ru/pogoda/");
            _pageHome.CityTextElement.SendKeys("New York");
            Thread.Sleep(100);
            _pageHome.CityTextElement.SendKeys(Keys.ArrowDown);
            _pageHome.CityTextElement.SendKeys(Keys.Enter);
        }
    }
}
